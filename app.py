from flask import Flask, request, jsonify
from flask_cors import CORS
import PyPDF2
import nltk
from nltk.corpus import stopwords
from transformers import pipeline

app = Flask(__name__)
CORS(app)

def extract_text_from_pdf(file_path):
    with open(file_path, 'rb') as file:
        print("@FILE_FILE@ => ")
        print(file)
        pdf_reader = PyPDF2.PdfReader(file)
        print("@PDF_READER@ => ")
        print(pdf_reader)
        text = ''
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()
    return text

def preprocess_text(text):
    stop_words = set(stopwords.words('english'))
    tokens = nltk.word_tokenize(text)
    tokens = [token.lower() for token in tokens if token.isalnum()]
    tokens = [token for token in tokens if token not in stop_words]
    return tokens

def answer_question(question, context):
    model = pipeline('question-answering')
    result = model(question=question, context=context)
    return result['answer']

@app.route('/qa', methods=['POST'])
def question_answering():
    file = request.files['file']
    print("FILE => ")
    print(file)
    
    question = request.form['question']
    print("QUESTION => ")
    print(question)
    file.save("1.pdf")
    text = extract_text_from_pdf("1.pdf")
    print("TEXT => ")
    print(text)
    
    preprocessed_text = preprocess_text(text)
    print("PPPPPreprocessed_text => ")
    print(preprocessed_text)
    
    answer = answer_question(question, ' '.join(preprocessed_text))

    return jsonify({'answer': answer})

if __name__ == '__main__':
    app.run()
